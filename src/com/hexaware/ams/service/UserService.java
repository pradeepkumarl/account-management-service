package com.hexaware.ams.service;

import java.util.Set;

import com.hexaware.ams.model.User;

public interface UserService {

	public User save(User user);

	public Set<User> findAll();

	public User findById(long userId);

	public void deleteById(long userId);

}
